﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CompanyQuickBooks.Startup))]
namespace CompanyQuickBooks
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }

        private void ConfigureAuth(IAppBuilder app)
        {

        }
    }
}
