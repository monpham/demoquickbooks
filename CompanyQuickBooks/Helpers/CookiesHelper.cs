﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Helpers
{
    public class CookiesHelper
    {
        public static void SET_COOKIES(string name = "default", string value = "")
        {
            var cookies = new HttpCookie(name) {
                    Expires = DateTime.Now.AddDays(30),
                    Value = value
                };            
            HttpContext.Current.Response.SetCookie(cookies);
            HttpContext.Current.Request.Cookies[name].Value = value;
        }
        public static string GET_COOKIES(string name = "default")
        {
            var cookies = HttpContext.Current.Request.Cookies[name] != null? HttpContext.Current.Request.Cookies[name].Value : null;
            return cookies;
        }
        public static void CLEAR_COOKIES(string name = "default")
        {
            var cookies = new HttpCookie(name)
            {
                Expires = DateTime.Now.AddDays(-1),
                Value = null
            };
            HttpContext.Current.Response.SetCookie(cookies);
        }
    }
}