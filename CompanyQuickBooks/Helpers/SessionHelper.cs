﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Helpers
{
    public class SessionHelper
    {
        public static void SetSession(string _namespace = "default", object _val = null)
        {            
            HttpContext.Current.Session[_namespace] = _val;            
        }

        public static object GetSession(string _namespace = "default")
        {
            return (HttpContext.Current.Session[_namespace] != null) ? HttpContext.Current.Session[_namespace] : null;
        }

        public static void ClearSession(string _namespace = "default")
        {            
            HttpContext.Current.Session[_namespace] = null;
            HttpContext.Current.Session.Contents.Remove(_namespace);
        }
    }
}