﻿using CompanyQuickBooks.Models;
using CompanyQuickBooks.Services;
using Helpers;
using Newtonsoft.Json.Linq;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CompanyQuickBooks.Controllers
{
    public class CompanyController : ApiController
    {
        private readonly ICompanyService _companyService = new CompanyService();

        [HttpGet]
        public HttpResponseMessage GetCompanies()
        {
            var result = new JObject();
            try
            {
                var accessToken = (TokenSession)SessionHelper.GetSession("company");
                var companiesInfo = _companyService.CompanyInfoQueryUsingoAuth(accessToken);
                if (!companiesInfo.HasErrors)
                    result = JObject.FromObject(new { success = true, data = companiesInfo.Target });
                else
                    result = JObject.FromObject(new { success = false, message = "Can't get data!" });
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { success = false, message = ex.Message });
            }
            return Request.CreateResponse(HttpStatusCode.InternalServerError, result);
        }
    }
}
