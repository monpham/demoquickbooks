﻿using CompanyQuickBooks.Models;
using DevDefined.OAuth.Consumer;
using DevDefined.OAuth.Framework;
using Helpers;
using System;
using System.Configuration;
using System.Web.Mvc;
using Utils;

namespace CompanyQuickBooks.Controllers
{
    public class HomeController : BaseMvcController
    {   
        private string consumerSecret, consumerKey, oauthLink, RequestToken, TokenSecret, oauth_callback_url;
        private string _oauthVerifyer, _realmid, _dataSource, _accessToken, _accessTokenSecret;
        
        // GET: Index
        public ActionResult Index()
        {
            var tokenSession = (TokenSession)SessionHelper.GetSession("company");
            if (tokenSession == null)
            {
                oauth_callback_url = Request.Url.GetLeftPart(UriPartial.Authority) + ConfigurationManager.AppSettings["oauth_callback_url"];
                consumerKey = ConfigurationManager.AppSettings["consumerKey"];
                consumerSecret = ConfigurationManager.AppSettings["consumerSecret"];
                oauthLink = Constants.OauthEndPoints.IdFedOAuthBaseUrl;
                IToken token = (IToken)Session["requestToken"];
                IOAuthSession session = CreateSession(oauthLink);
                IToken requestToken = session.GetRequestToken();
                Session["requestToken"] = requestToken;
                RequestToken = requestToken.Token;
                TokenSecret = requestToken.TokenSecret;

                oauthLink = Constants.OauthEndPoints.AuthorizeUrl + "?oauth_token=" + RequestToken + "&oauth_callback=" + UriUtility.UrlEncode(oauth_callback_url);
                return Redirect(oauthLink);
            }
            return RedirectToAction("CompanyInfo");
        }

        public ActionResult CompanyInfo()
        {
            var tokenSession = (TokenSession)SessionHelper.GetSession("company");
            if (tokenSession == null)
            {
                // This value is used to Get Access Token.
                _oauthVerifyer = Request.QueryString["oauth_verifier"].ToString();

                TokenSession _tokenSession = new TokenSession();

                _realmid = Request.QueryString["realmId"].ToString();

                //If dataSource is QBO call QuickBooks Online Services, else call QuickBooks Desktop Services
                _dataSource = Request.QueryString["dataSource"].ToString();

                IOAuthSession clientSession = CreateSessionRequest();
                try
                {
                    IToken accessToken = clientSession.ExchangeRequestTokenForAccessToken((IToken)Session["requestToken"], _oauthVerifyer);
                    _accessToken = accessToken.Token;
                    _accessTokenSecret = accessToken.TokenSecret;
                }
                catch (Exception ex)
                {
                    //Handle Exception if token is rejected or exchange of Request Token for Access Token failed.
                    return RedirectToAction("Index");
                }

                _tokenSession.AccessToken = _accessToken;
                _tokenSession.AccessTokenSecret = _accessTokenSecret;
                _tokenSession.RealmId = _realmid;
                _tokenSession.DataSource = _dataSource;

                SessionHelper.SetSession("company", _tokenSession);
            }
            
            return View();
        }
                                
    }
}