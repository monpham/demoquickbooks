﻿using System.Web;
using System.Web.Optimization;

namespace CompanyQuickBooks
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/bundles/bootstrap/css").Include(
                      "~/Assets/includes/bootstrap/css/bootstrap.css"));

            bundles.Add(new StyleBundle("~/bundles/core/css").Include(
                      "~/Assets/includes/core/css/Site.css"));

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Assets/includes/jquery/01_jquery.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap/js").Include(
                        "~/Assets/includes/bootstrap/js/bootstrap.js"));
            
            bundles.Add(new ScriptBundle("~/bundles/angular").Include(
                        "~/Assets/includes/angular/01angular-1.6.3.min.js",
                        "~/Assets/includes/angular/01angular-route.min.js",
                        "~/Assets/includes/angular/02angular-sanitize.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/scripts").Include(
                        "~/Assets/scripts/configs/app.js",
                        "~/Assets/scripts/configs/config.js",
                        "~/Assets/scripts/services/01requestApi.js",
                        "~/Assets/scripts/services/baseService.js",
                        "~/Assets/scripts/controllers/homeCtrl.js"));

#if DEBUG
            BundleTable.EnableOptimizations = false;
#else
            BundleTable.EnableOptimizations = true;
#endif    
        }
    }
}
