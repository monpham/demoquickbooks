﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace CompanyQuickBooks
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("fonts*.woff");
            routes.IgnoreRoute("*.js");
            routes.IgnoreRoute("*.html");
            routes.IgnoreRoute("*.css");
            routes.IgnoreRoute("api/*");

            routes.MapRoute(
                name: "OAuthResponse",
                url: "home",
                defaults: new { controller = "Home", action = "CompanyInfo" }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}",
                defaults: new { controller = "Home", action = "Index" }
            );
        }
    }
}
