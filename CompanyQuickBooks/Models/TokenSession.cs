﻿using Antlr.Runtime;

namespace CompanyQuickBooks.Models
{
    public class TokenSession
    {
        public IToken RequestToken { get; set; }
        public string AccessToken { get; set; }
        public string AccessTokenSecret { get; set; }
        public string RealmId { get; set; }
        public string DataSource { get; set; }
    }
}