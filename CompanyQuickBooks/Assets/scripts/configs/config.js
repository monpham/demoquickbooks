angular.module('companyApp').config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
    $routeProvider.when('/', {
        templateUrl: 'Home/Index'
    })
    .when('/home', {
        templateUrl: 'Home/CompanyInfo'
    })
    .otherwise({
        redirectTo: '/'
    });    
    $locationProvider.html5Mode(true);
}]);   