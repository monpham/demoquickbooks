'use strict';

angular.module('companyApp', ['ngRoute', 'ngSanitize']);

angular.element(document).ready(function () {
    angular.bootstrap(document.documentElement, ['companyApp']);
});