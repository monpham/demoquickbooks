﻿angular.module('companyApp').controller('homeCtrl', ['$scope', 'baseService', 'requestApiHelper', function ($scope, baseService, requestApiHelper) {

    baseService.GET(requestApiHelper.API.GET_COMPANIES).then(function (response) {
        console.log(response);
    }, function (err) {
        console.log(err);
    });

}]);