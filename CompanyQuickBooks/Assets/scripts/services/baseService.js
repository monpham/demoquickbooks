angular.module('companyApp').factory('baseService', ['$http', '$q', '$window', function ($http, $q, $window) {
    
    return angular.extend($http, {
        URL_HOST: window.location.origin.concat('/'),        
        WebsiteName: ' | QuickBooks',       
        GET: function(controller){
            var result = $q.defer();
            $http.get(controller).then(function (response){
                result.resolve(response.data); 
            }, result.reject.bind(null));
            
            return result.promise;
        },
        POST: function(controller, param){
            var result = $q.defer();            
            $http.defaults.headers.post['Content-Type'] = 'application/json';
            $http({
                method: 'POST',
                url: controller,
                data: param   
            }).then(function(response){
                result.resolve(response.data);
            }, result.reject.bind(null));
                        
            return result.promise;
        }                
    });
}]);