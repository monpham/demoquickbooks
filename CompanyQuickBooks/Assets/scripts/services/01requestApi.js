angular.module('companyApp').factory('requestApiHelper', function () {
    return {
        API: {            
            GET_COMPANIES: 'api/Company/GetCompanies'
        }
    };
});