﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intuit.Ipp.Security;
using Intuit.Ipp.Core;
using System.Configuration;
using System.Net;
using System.Globalization;
using System.IO;
using Intuit.Ipp.Exception;
using Intuit.Ipp.Data;
using CompanyQuickBooks.Models;

namespace Services.QBO
{
    public class QBOServiceInitializer
    {

        private static string accessToken = string.Empty;
        private static string accessTokenSecret = string.Empty;
        private static string consumerKey = string.Empty;
        private static string consumerSecret = string.Empty;
        private static string realmId = string.Empty;
        private static string appToken = string.Empty;
        private static string baseUrl = string.Empty;

        private static void Initialize()
        {
            //Add keys in web.config
            consumerKey = ConfigurationManager.AppSettings["ConsumerKey"];
            consumerSecret = ConfigurationManager.AppSettings["ConsumerSecret"];            
            appToken = ConfigurationManager.AppSettings["applicationToken"];
            baseUrl = ConfigurationManager.AppSettings["ServiceContext.BaseUrl.Qbo"];
        }

        internal static ServiceContext InitializeQBOServiceContextUsingoAuth(TokenSession session)
        {
            Initialize();
            OAuthRequestValidator reqValidator = new OAuthRequestValidator(session.AccessToken, session.AccessTokenSecret, consumerKey, consumerSecret);
            ServiceContext context = new ServiceContext(session.RealmId, IntuitServicesType.QBO, reqValidator);

            //MinorVersion represents the latest features/fields in the xsd supported by the QBO apis.
            //Read more details here- https://developer.intuit.com/docs/0100_quickbooks_online/0200_dev_guides/accounting/querying_data                        
            context.IppConfiguration.MinorVersion.Qbo = "8";
            context.IppConfiguration.BaseUrl.Qbo = baseUrl;
            context.IppConfiguration.Logger.RequestLog.EnableRequestResponseLogging = true;
            context.IppConfiguration.Logger.RequestLog.ServiceRequestLoggingLocation = @"D:\MyJobs\Outsource\Projects\CompanyQuickBooks\CompanyQuickBooks\App_Data";
            context.IppConfiguration.Message.Response.SerializationFormat = Intuit.Ipp.Core.Configuration.SerializationFormat.Json;

            return context;
        }
    }
}
