﻿using System;
using System.Collections.Generic;
using Intuit.Ipp.Core;
using Intuit.Ipp.Data;
using Intuit.Ipp.QueryFilter;
using Intuit.Ipp.DataService;
using Intuit.Ipp.Exception;
using System.Collections.ObjectModel;
using System.Linq;
using Services.Result;
using Services.QBO;
using Helpers;
using CompanyQuickBooks.Models;

namespace CompanyQuickBooks.Services
{
    public interface ICompanyService
    {
        DataServiceResult<IList<CompanyInfo>> CompanyInfoQueryUsingoAuth(TokenSession session);
    }
    public class CompanyService : ICompanyService
    {        
        public DataServiceResult<IList<CompanyInfo>> CompanyInfoQueryUsingoAuth(TokenSession session)
        {
            ServiceContext qboContextoAuth = QBOServiceInitializer.InitializeQBOServiceContextUsingoAuth(session);
            var result = new DataServiceResult<IList<CompanyInfo>>();
            qboContextoAuth.RequestId = Helper.GetGuid();             
            QueryService<CompanyInfo> entityQuery = new QueryService<CompanyInfo>(qboContextoAuth);
            List<CompanyInfo> comp = entityQuery.ExecuteIdsQuery("SELECT * FROM CompanyInfo").ToList<CompanyInfo>();
            result.Target = comp;
            return result;
        }
    }
}