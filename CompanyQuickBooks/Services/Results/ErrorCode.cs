﻿namespace Services.Result
{
    public enum ErrorCode
    {
        None = 0,
        Exception = 1,
        Empty = 2        
    }
}
