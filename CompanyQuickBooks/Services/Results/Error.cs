﻿using System;
using System.Collections.Generic;

namespace Services.Result
{
    public class Error
    {
        public ErrorCode Code { get; set; }           
        public string Message { get; set; }     

        public Error()
        {            
            Code = ErrorCode.None;
            Message = string.Empty;
        }

        public Error(ErrorCode code, string message)
        {
            Code = code;
            Message = message;
        }
    }

    public class ErrorHelper
    {
        public List<Error> Errors { get; set; }

        public ErrorHelper()
        {
            Errors = new List<Error>();
        }

        public List<Error> AddErrorRange(IEnumerable<Error> ErrorsRange)
        {
            Errors.AddRange(ErrorsRange);
            return Errors;
        }

        public List<Error> AddError(ErrorCode error, string message)
        {
            Errors.Add(new Error { Code = error, Message = message });
            return Errors;
        }
                
        public List<Error> AddError(ErrorCode error)
        {
            return AddError(error, null);
        }
               
        public List<Error> AddError(string exceptionMessage)
        {
            return AddError(ErrorCode.Exception, exceptionMessage);
        }
    }
}
